const theme = {
  colors: {
    white1: '#FFF',
    grey1: '#EEE',
    grey2: '#999',
    grey3: '#ccc',
    grey4: '#e7e7e7',
    grey5: '#E0E7EE',
    orange1: '#FF7800',
    black1: '#212122',
    red1: '#ff0000',
  },
  shadow: '1px 1px 5px 0 rgba(0,0,29,0.22)',
  borderRadius: '3px',
  margin: '20px',
}

export default theme
