import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  *,
  *::after,
  *::before {
    padding: 0;
    margin: 0;
    box-sizing: inherit;
  }

  html {
    box-sizing: border-box;
    height: 100%;
    scroll-behavior: smooth;
  }

  body {
    background: ${({ theme }) => theme.colors.grey1};
    font-family: Helvetica, Sans-Serif;
    height: 100%;
  }

  a {
    text-decoration: none;
  }

  button {
    border: none;
  }

  a, button, input {
    outline: 0;
  }
`

export default GlobalStyle
