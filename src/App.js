import React from 'react'
import { ThemeProvider } from 'styled-components'

import { ProductProvider, PaymentProvider } from './contexts'
import GlobalStyle from './styles/globalStyle'
import theme from './styles/theme'
import Routes from './routes'

const App = () => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    <ProductProvider>
      <PaymentProvider>
        <Routes />
      </PaymentProvider>
    </ProductProvider>
  </ThemeProvider>
)

export default App
