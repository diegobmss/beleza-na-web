import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'

import Navbar from './components/Navbar/Navbar'

import CarryBag from './pages/carry-bag/CarryBag'
import Confirmation from './pages/confirmation/Confirmation'
import Payment from './pages/payment/Payment'

import { ROUTES } from './constants/urls'

const Routes = () => {
  const { app } = ROUTES

  return (
    <BrowserRouter>
      <Navbar
        options={[
          { name: app.carryBag.name, link: app.carryBag.url },
          { name: app.payment.name, link: app.payment.url },
          { name: app.confirmation.name, link: app.confirmation.url },
        ]}
      />
      <Switch>
        <Route path={app.carryBag.url}>
          <CarryBag />
        </Route>
        <Route path={app.payment.url}>
          <Payment />
        </Route>
        <Route path={app.confirmation.url}>
          <Confirmation />
        </Route>
        <Route path={app.url}>
          <Redirect to={app.carryBag.url} />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default Routes
