import React from 'react'
import { useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'

import Button from '../../components/Button/Button'
import Card from '../../components/Card/Card'
import Container from '../../components/Container/Container'
import Loading from '../../components/Loading/Loading'
import PaymentForm from '../../components/PaymentForm/PaymentForm'
import Summary from '../../components/Summary/Summary'

import { ROUTES } from '../../constants/urls'
import { MESSAGES } from '../../constants/messages'

import { useProductContext, usePaymentContext } from '../../contexts'

import * as S from './Payment.styles'

const schema = Yup.object().shape({
  cardNumber: Yup.string()
    .min(19, MESSAGES.cardField)
    .max(19, MESSAGES.cardField)
    .required(MESSAGES.requiredField),
  cardName: Yup.string().required(MESSAGES.requiredField),
  valid: Yup.string()
    .min(7, MESSAGES.validField)
    .max(7, MESSAGES.validField)
    .required(MESSAGES.requiredField),
  cvv: Yup.string()
    .min(3, MESSAGES.cvvField)
    .max(3, MESSAGES.cvvField)
    .required(MESSAGES.requiredField),
})

const Payment = () => {
  const { app } = ROUTES
  const history = useHistory()
  const { products, isLoading } = useProductContext()
  const { setCreditCard } = usePaymentContext()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmit = (data) => {
    setCreditCard(data)
    history.push(app.confirmation.url)
  }

  if (isLoading) {
    return <Loading />
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container>
        <S.Grid>
          <Card title='Cartão de Crédito'>
            <PaymentForm register={register} errors={errors} />
          </Card>
          <Summary
            subTotal={products?.subTotal}
            shippingTotal={products?.shippingTotal}
            discount={products?.discount}
            total={products?.total}
          />
        </S.Grid>
        <S.Button>
          <Button type='submit'>Finalizar o pedido</Button>
        </S.Button>
      </Container>
    </form>
  )
}

export default Payment
