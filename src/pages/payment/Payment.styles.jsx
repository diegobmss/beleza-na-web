import styled from 'styled-components'
import media from 'styled-media-query'

export const Grid = styled.div`
  display: block;

  > :nth-child(n):not(:last-child) {
    margin-bottom: ${({ theme }) => theme.margin};
  }

  ${media.greaterThan('medium')`
    display: grid;
    gap: ${({ theme }) => theme.margin};
    grid-template-columns: 1fr 25%;
  `}
`

export const Button = styled.div`
  ${media.greaterThan('medium')`
    width: 100%;
    text-align: right;
  `}
`
