import styled from 'styled-components'
import media from 'styled-media-query'

export const Grid = styled.div`
  display: block;

  > :nth-child(n):not(:last-child) {
    margin-bottom: ${({ theme }) => theme.margin};
  }

  ${media.greaterThan('medium')`
    margin-top: ${({ theme }) => theme.margin};
    display: grid;
    gap: ${({ theme }) => theme.margin};
    grid-template-columns: 1fr 25%;
  `}
`
