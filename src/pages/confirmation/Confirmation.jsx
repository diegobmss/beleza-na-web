import React from 'react'
import { Redirect } from 'react-router-dom'
import shortid from 'shortid'

import Card from '../../components/Card/Card'
import Container from '../../components/Container/Container'
import Loading from '../../components/Loading/Loading'
import PaymentCard from '../../components/PaymentCard/PaymentCard'
import ProductItem from '../../components/ProductItem/ProductItem'
import Purchased from '../../components/Purchased/Purchased'
import Summary from '../../components/Summary/Summary'

import { useProductContext, usePaymentContext } from '../../contexts'

import { ROUTES } from '../../constants/urls'

import * as S from './Confirmation.styles'

const Confirmation = () => {
  const { app } = ROUTES
  const { products, isLoading } = useProductContext()
  const { creditCard } = usePaymentContext()

  if (!creditCard.cardNumber) {
    return <Redirect to={app.carryBag.url} />
  }

  if (isLoading) {
    return <Loading />
  }

  return (
    <Container>
      <Purchased />
      <Card title='Pagamento'>
        <PaymentCard
          key={shortid.generate()}
          cardNumber={creditCard.cardNumber}
          cardName={creditCard.cardName}
          valid={creditCard.valid}
        />
      </Card>
      <S.Grid>
        <Card title='Produtos'>
          {products.items.map(({ product }) => (
            <ProductItem
              key={shortid.generate()}
              imgSrc={product.imageObjects[0].medium}
              name={product.name}
              price={product.priceSpecification.price}
            />
          ))}
        </Card>
        <Summary
          subTotal={products.subTotal}
          shippingTotal={products.shippingTotal}
          discount={products.discount}
          total={products.total}
        />
      </S.Grid>
    </Container>
  )
}

export default Confirmation
