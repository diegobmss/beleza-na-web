import React from 'react'
import shortid from 'shortid'

import Button from '../../components/Button/Button'
import Card from '../../components/Card/Card'
import Container from '../../components/Container/Container'
import Loading from '../../components/Loading/Loading'
import ProductItem from '../../components/ProductItem/ProductItem'
import Summary from '../../components/Summary/Summary'

import { ROUTES } from '../../constants/urls'

import { useProductContext } from '../../contexts'

import * as S from './CarryBag.styles'

const CarryBag = () => {
  const { app } = ROUTES
  const { products, isLoading } = useProductContext()

  if (isLoading) {
    return <Loading />
  }

  return (
    <Container>
      <S.Grid>
        <Card title='Produtos'>
          {products?.items?.map(({ product }) => (
            <ProductItem
              key={shortid.generate()}
              imgSrc={product.imageObjects[0].medium}
              name={product.name}
              price={product.priceSpecification.price}
            />
          ))}
        </Card>
        <Summary
          subTotal={products?.subTotal}
          shippingTotal={products?.shippingTotal}
          discount={products?.discount}
          total={products?.total}
        />
      </S.Grid>
      <S.Button>
        <Button to={app.payment.url}>Seguir para o pagamento</Button>
      </S.Button>
    </Container>
  )
}

export default CarryBag
