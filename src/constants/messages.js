export const MESSAGES = {
  requiredField: 'Campo obrigatório',
  cardField: 'O campo deve 16 números',
  validField: 'O campo deve conter mês e ano',
  cvvField: 'O campo deve conter 3 dígitos',
}
