export const ROUTES = {
  app: {
    url: '/',
    carryBag: {
      name: 'Sacola',
      url: '/sacola',
    },
    payment: {
      name: 'Pagamento',
      url: '/pagamento',
    },
    confirmation: {
      name: 'Confirmação',
      url: '/confirmacao',
    },
  },
}

export const LOGO =
  'https://res.cloudinary.com/beleza-na-web/image/upload/f_svg,fl_progressive,q_auto:eco/v1/blz/assets-store/0.0.309/images/store/1/logo.svg'
