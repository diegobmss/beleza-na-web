import React from 'react'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'

import HalfField from '../Form/HalfField/HalfField'
import InputField from '../Form/InputField/InputField'

import * as S from './PaymentForm.styles'

const PaymentForm = ({ register, errors }) => (
  <>
    <InputField label='Número do cartão:' error={errors.cardNumber}>
      <InputMask
        mask='9999.9999.9999.9999'
        maskChar={null}
        {...register('cardNumber')}
      >
        {() => (
          <S.Input
            placeholder='____.____.____.____'
            type='text'
            {...register('cardNumber')}
          />
        )}
      </InputMask>
    </InputField>
    <InputField label='Nome do Titular:' error={errors.cardName}>
      <S.Input
        placeholder='Como no cartão'
        type='text'
        {...register('cardName')}
      />
    </InputField>
    <HalfField>
      <InputField label='Validade (mês/ano):' error={errors.valid}>
        <InputMask mask='99/9999' maskChar={null} {...register('valid')}>
          {() => (
            <S.Input placeholder='__/____' type='text' {...register('valid')} />
          )}
        </InputMask>
      </InputField>
      <InputField label='CVV:' error={errors.cvv}>
        <InputMask mask='999' maskChar={null} {...register('cvv')}>
          {() => <S.Input placeholder='___' type='text' {...register('cvv')} />}
        </InputMask>
      </InputField>
    </HalfField>
  </>
)

PaymentForm.propTypes = {
  register: PropTypes.func.isRequired,
  errors: PropTypes.shape({
    cardNumber: PropTypes.shape({
      message: PropTypes.string.isRequired,
    }),
    cardName: PropTypes.shape({
      message: PropTypes.string.isRequired,
    }),
    valid: PropTypes.shape({
      message: PropTypes.string.isRequired,
    }),
    cvv: PropTypes.shape({
      message: PropTypes.string.isRequired,
    }),
  }).isRequired,
}

export default PaymentForm
