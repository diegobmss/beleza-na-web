import styled from 'styled-components'

export const Input = styled.input`
  background-color: ${({ theme }) => theme.colors.white1};
  border: 1px solid ${({ theme }) => theme.colors.grey4};
  box-shadow: inset 0 1px 2px 0 rgba(0, 0, 0, 0.2);
  border-radius: 3px;
  width: 100%;
  height: 45px;
  font-size: 16px;
  line-height: 19px;
  padding-left: 13px;
  color: ${({ theme }) => theme.colors.black1};

  ::placeholder {
    color: ${({ theme }) => theme.colors.grey5};
    opacity: 1;
  }
`
