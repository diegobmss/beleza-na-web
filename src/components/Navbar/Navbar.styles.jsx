import styled from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  background: ${({ theme }) => theme.colors.white1};
  padding: 12px;
  border-radius: ${({ theme }) => theme.borderRadius};
  box-shadow: ${({ theme }) => theme.shadow};

  ${media.greaterThan('medium')`
  padding: 24px;
    justify-content: flex-end;

    > :nth-child(n):not(:last-child) {
      margin-right: 30px;
    }
  `}
`

export const NotAllowed = styled.div`
  cursor: not-allowed;
`
