import React from 'react'
import shortid from 'shortid'
import PropTypes from 'prop-types'
import { useLocation, Link } from 'react-router-dom'

import { Item } from '../Typography/Typography'
import * as S from './Navbar.styles'

const Navbar = ({ options }) => {
  let controlStep = false
  return (
    <S.Container>
      {options.map((option) => {
        if (useLocation().pathname === option.link) {
          controlStep = true
          return (
            <Link key={shortid.generate()} to={option.link}>
              <Item isActive>{option.name}</Item>
            </Link>
          )
        }

        if (controlStep === false) {
          return (
            <Link key={shortid.generate()} to={option.link}>
              <Item>{option.name}</Item>
            </Link>
          )
        }

        return (
          <S.NotAllowed>
            <Item>{option.name}</Item>
          </S.NotAllowed>
        )
      })}
    </S.Container>
  )
}

Navbar.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      link: PropTypes.string,
    })
  ).isRequired,
}

export default Navbar
