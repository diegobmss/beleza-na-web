import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import * as S from './Button.styles'

const Button = ({ to, type, children }) =>
  to !== '' ? (
    <Link to={to}>
      <S.Button type={type}>{children}</S.Button>
    </Link>
  ) : (
    <S.Button type={type}>{children}</S.Button>
  )

Button.defaultProps = {
  to: '',
  type: 'button',
}

Button.propTypes = {
  to: PropTypes.string,
  type: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default Button
