import styled from 'styled-components'
import media from 'styled-media-query'

export const Button = styled.button`
  background: ${({ theme }) => theme.colors.orange1};
  box-shadow: inset 0 -3px 0 0 #d45a00, 0 2px 4px 0 rgba(0, 0, 0, 0.25);
  border-radius: ${({ theme }) => theme.borderRadius};
  padding: 18px 5px;

  color: ${({ theme }) => theme.colors.white1};
  font-size: 20px;
  font-weight: bold;
  letter-spacing: 0.5px;
  line-height: 24px;
  text-align: center;
  text-transform: uppercase;
  width: 100%;
  cursor: pointer;

  ${media.greaterThan('medium')`
    max-width: 350px;
  `}
`
