import styled from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.grey3};
  padding: 15px 14px;

  ${media.greaterThan('medium')`
    margin-top: 22px;
    margin-bottom: ${({ theme }) => theme.margin};

    border: none;
    background: ${({ theme }) => theme.colors.white1};
    border-radius: ${({ theme }) => theme.borderRadius};
    box-shadow: ${({ theme }) => theme.shadow};
    padding: 13px;
  `}
`

export const Item = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 9px;

  :nth-last-child(-n + 2) {
    margin-bottom: 16px;
  }

  :last-child {
    margin-bottom: 0px;
  }
`
