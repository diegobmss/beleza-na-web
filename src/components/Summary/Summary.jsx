import React from 'react'
import PropTypes from 'prop-types'

import { Price, PriceBold, Title, TitleBold } from '../Typography/Typography'

import { useFormatCoin } from '../../hooks/useFormatCoin'

import * as S from './Summary.styles'

const Summary = ({ subTotal, shippingTotal, discount, total }) => (
  <S.Container>
    <S.Item>
      <Title>Produtos</Title>
      <Price>{useFormatCoin(subTotal)}</Price>
    </S.Item>
    <S.Item>
      <Title>Frete</Title>
      <Price>{useFormatCoin(shippingTotal)}</Price>
    </S.Item>
    <S.Item>
      <Title color='orange1'>Desconto</Title>
      <Price color='orange1'>- {useFormatCoin(discount)}</Price>
    </S.Item>
    <S.Item>
      <TitleBold>Total</TitleBold>
      <PriceBold>{useFormatCoin(total)}</PriceBold>
    </S.Item>
  </S.Container>
)

Summary.propTypes = {
  subTotal: PropTypes.number.isRequired,
  shippingTotal: PropTypes.number.isRequired,
  discount: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
}

export default Summary
