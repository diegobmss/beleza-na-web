import styled from 'styled-components'

export const Container = styled.div`
  border-radius: ${({ theme }) => theme.borderRadius};
  border: 1px solid ${({ theme }) => theme.colors.grey1};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 12px;
  margin-bottom: 15px;

  :last-child {
    margin-bottom: 0px;
  }
`

export const Image = styled.img`
  max-width: 65px;
  width: 100%;
  margin-right: 11px;
`

export const Content = styled.div``

export const Price = styled.div`
  margin-top: 15px;
  text-align: right;
`
