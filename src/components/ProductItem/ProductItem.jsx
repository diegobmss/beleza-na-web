import React from 'react'
import PropTypes from 'prop-types'

import { SubTitle, PriceBold } from '../Typography/Typography'

import { useFormatCoin } from '../../hooks/useFormatCoin'

import * as S from './ProductItem.styles'

const ProductItem = ({ imgSrc, name, price }) => (
  <S.Container>
    <S.Image src={imgSrc} alt={name} />
    <S.Content>
      <SubTitle>{name}</SubTitle>
      <S.Price>
        <PriceBold>{useFormatCoin(price)}</PriceBold>
      </S.Price>
    </S.Content>
  </S.Container>
)

ProductItem.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
}

export default ProductItem
