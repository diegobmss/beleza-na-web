import React from 'react'
import PropTypes from 'prop-types'

import { Title } from '../Typography/Typography'

import * as S from './PaymentCard.styles'

const PaymentCard = ({ cardNumber, cardName, valid }) => (
  <S.Container>
    <Title>****.****.****.{cardNumber.substr(cardNumber.length - 4)}</Title>
    <Title>{cardName}</Title>
    <Title>{valid}</Title>
  </S.Container>
)

PaymentCard.propTypes = {
  cardNumber: PropTypes.string.isRequired,
  cardName: PropTypes.string.isRequired,
  valid: PropTypes.string.isRequired,
}

export default PaymentCard
