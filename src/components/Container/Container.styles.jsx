import styled from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
  padding: 12px;

  > :nth-child(n):not(:last-child) {
    margin-bottom: ${({ theme }) => theme.margin};
  }

  ${media.greaterThan('medium')`
    max-width: 1000px;
    margin: 0 auto;

    > :nth-child(n):not(:last-child) {
      margin-bottom: 0px;
    }
  `}
`
