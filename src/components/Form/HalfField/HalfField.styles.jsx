import styled from 'styled-components'

export const Container = styled.div`
  display: grid;
  gap: ${({ theme }) => theme.margin};
  grid-template-columns: repeat(2, 1fr);
`
