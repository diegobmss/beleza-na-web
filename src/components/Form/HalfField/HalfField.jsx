import React from 'react'
import PropTypes from 'prop-types'

import * as S from './HalfField.styles'

const HalfField = ({ children }) => <S.Container>{children}</S.Container>

HalfField.propTypes = {
  children: PropTypes.node.isRequired,
}

export default HalfField
