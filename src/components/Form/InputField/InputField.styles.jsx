import styled from 'styled-components'

export const InputField = styled.div`
  margin-bottom: 25px;

  :last-child {
    margin-bottom: 0px;
  }
`

export const Label = styled.p`
  color: ${({ theme }) => theme.colors.grey3};
  font-size: 12px;
  font-weight: bold;
  line-height: 14px;
  margin-bottom: 5px;
  margin-left: 2px;
`

export const Error = styled.span`
  color: ${({ theme }) => theme.colors.red1};
  display: block;
  font-size: 12px;
  line-height: 14px;
  margin-top: 5px;
  margin-left: 2px;
`
