import React from 'react'
import PropTypes from 'prop-types'

import * as S from './InputField.styles'

const InputField = ({ label, error, children }) => (
  <S.InputField>
    {label && <S.Label>{label}</S.Label>}
    {children}
    {error && <S.Error>{error.message}</S.Error>}
  </S.InputField>
)

InputField.propTypes = {
  label: PropTypes.string,
  error: PropTypes.shape({ message: PropTypes.string }),
  children: PropTypes.node.isRequired,
}

InputField.defaultProps = {
  label: '',
  error: {},
}

export default InputField
