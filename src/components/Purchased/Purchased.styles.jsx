import styled from 'styled-components'

export const Container = styled.div`
  text-align: center;
`

export const Image = styled.img`
  max-width: 40px;
  width: 100%;
  height: auto;
  margin-bottom: 10px;
`
