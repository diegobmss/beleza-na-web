import React from 'react'

import { TitleBold } from '../Typography/Typography'

import successIcon from '../../assets/icons/success.png'

import * as S from './Purchased.styles'

const Purchased = () => (
  <S.Container>
    <S.Image src={successIcon} alt='Sucesso' />
    <TitleBold color='orange1'>Compra efetuada com sucesso</TitleBold>
  </S.Container>
)

export default Purchased
