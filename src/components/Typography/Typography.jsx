import styled from 'styled-components'
import media from 'styled-media-query'

export const Item = styled.p`
  color: ${({ isActive, theme }) =>
    isActive ? theme.colors.orange1 : theme.colors.grey1};
  font-size: 13px;
  font-weight: bold;
  line-height: 16px;
  text-transform: uppercase;

  ${media.greaterThan('medium')`
    font-size: 14px;
    line-height: 16px;
  `}

  ${media.greaterThan('large')`
    font-size: 16px;
    line-height: 16px;
  `}
`

export const Title = styled.h1`
  color: ${({ color, theme }) => theme.colors[color] ?? theme.colors.black1};
  font-size: 14px;
  font-weight: normal;
  line-height: 17px;
  text-transform: uppercase;
`

export const TitleBold = styled(Title)`
  font-weight: bold;
`

export const SubTitle = styled.h2`
  color: ${({ theme }) => theme.colors.black1};
  font-size: 13px;
  font-weight: normal;
  line-height: 16px;
`

export const Price = styled.h3`
  color: ${({ color, theme }) => theme.colors[color] ?? theme.colors.black1};
  font-size: 14px;
  font-weight: normal;
  line-height: 17px;
`

export const PriceBold = styled(Price)`
  font-weight: bold;
`
