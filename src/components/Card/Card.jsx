import React from 'react'
import PropTypes from 'prop-types'

import { TitleBold } from '../Typography/Typography'

import * as S from './Card.styles'

const Card = ({ title, children }) => (
  <S.Container>
    <S.Margin>
      <TitleBold color='grey2'>{title}</TitleBold>
    </S.Margin>
    <S.Card>{children}</S.Card>
  </S.Container>
)

Card.defaultProps = {
  title: '',
}

Card.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default Card
