import styled from 'styled-components'

export const Container = styled.div``

export const Margin = styled.div`
  margin: 0px 10px 5px 10px;
`

export const Card = styled.div`
  background: ${({ theme }) => theme.colors.white1};
  border-radius: ${({ theme }) => theme.borderRadius};
  box-shadow: ${({ theme }) => theme.shadow};
  padding: 13px;
`
