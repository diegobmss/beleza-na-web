import React from 'react'

import * as S from './Loading.styles'

const Loading = () => (
  <S.Container>
    <S.Loading />
  </S.Container>
)

export default Loading
