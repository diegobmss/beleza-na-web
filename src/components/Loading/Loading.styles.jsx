import styled, { keyframes } from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
`

const spinEffect = keyframes`
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
`

export const Loading = styled.div`
  border: 16px solid ${({ theme }) => theme.colors.white1};
  border-top: 16px solid ${({ theme }) => theme.colors.orange1};
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: ${spinEffect} 2s linear infinite;
`
