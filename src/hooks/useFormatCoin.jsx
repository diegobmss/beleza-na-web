export const useFormatCoin = (value) =>
  value.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
