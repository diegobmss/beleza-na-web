import React, {
  createContext,
  useContext,
  useCallback,
  useEffect,
  useState,
} from 'react'
import PropTypes from 'prop-types'
import api from '../services/api'
import { GET_PRODUCTS } from '../constants/endpoints'

const defaultValue = {
  products: {},
  isLoading: true,
}

export const ProductContext = createContext(defaultValue)

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState(defaultValue.products)
  const [isLoading, setLoading] = useState(defaultValue.isLoading)

  const fetchProducts = useCallback(async () => {
    const { data } = await api.get(GET_PRODUCTS)
    setProducts(data)
    setLoading(false)
  }, [])

  useEffect(() => {
    setLoading(true)
    fetchProducts()
  }, [fetchProducts])

  return (
    <ProductContext.Provider
      value={{
        products,
        isLoading,
      }}
    >
      {children}
    </ProductContext.Provider>
  )
}

ProductProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

export const useProductContext = () => useContext(ProductContext)
