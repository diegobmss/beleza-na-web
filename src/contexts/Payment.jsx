import React, { createContext, useContext, useState } from 'react'
import PropTypes from 'prop-types'

const defaultValue = {
  payment: {},
}

export const PaymentContext = createContext(defaultValue)

export const PaymentProvider = ({ children }) => {
  const [creditCard, setCreditCard] = useState(defaultValue.payment)

  return (
    <PaymentContext.Provider
      value={{
        creditCard,
        setCreditCard,
      }}
    >
      {children}
    </PaymentContext.Provider>
  )
}

PaymentProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

export const usePaymentContext = () => useContext(PaymentContext)
